function showError($field, $error, $textError) {
    $field.addClass("errorField");
    $error.text($textError);
    $error.show();
}

function hideError($field, $error, $textError) {
    $error.text("Field must be filled out");
    $field.removeClass("errorField");
    $error.hide();
}

$(document).ready(function () {
    $('#submitButton').on("click", function () {
        var hasError = false;
        $('.block-1 .form-group').each(function () {
            var $this = $(this);
            var $input = $this.find('input,select');
            var $error = $this.find('.errorMessage');
            if ($input.val() == "") {
                showError($input, $error, "Field must be filled out");
                hasError = true;
            } else
                hideError($input,$error);
        });
        $('.block-2 .form-group').each(function () {
            var $this = $(this);
            var $input = $this.find('input');
            var $error = $this.find('.errorMessage');
            if ($input.attr('name') == 'phoneNumber') {
                if ($input.val() == "") {
                    showError($input, $error, "Field must be filled out");
                    hasError = true;
                } else {
                    var matches = $input.val().match(/^\+(\d{1,3})[-](\d{1,3})[-](\d{1,7})/);
                    if (matches == null) {
                        showError($input, $error, "Field must have phone format");
                        hasError = true;
                    } else
                        hideError($input, $error, "Field must be filled out");
                }
            }
            if ($input.attr('name') == 'inputBirthDate') {
                if ($input.val() == "") {
                    showError($input, $error, "Field must be filled out");
                    hasError = true;
                } else {
                    var matches = $input.val().match(/^(\d{2})\.(\d{2})\.(\d{4})$/);
                    if (matches == null) {
                        showError($input, $error, "Field must be in date format");
                        hasError = true;
                    } else
                        hideError($input, $error, "Field must be filled out");
                }
            }
            if ($input.val() == "") {
                showError($input, $error, "Field must be filled out");
                hasError = true;
            } else
                hideError($input, $error, "Field must be filled out");
        });

        $('.block-3 .form-group').each(function () {
            var $this = $(this);
            var $textarea = $this.find('textarea');
            var $error = $this.find('.errorMessage');
            if ($textarea.val() == "") {
                showError($textarea, $error, "Field must be filled out");
                hasError = true;
            } else
                hideError($textarea, $error, "Field must be filled out");
        });
        $('.block-4').each(function () {
            var $this = $(this);
            var $input = $this.find('input');
            var $error = $this.find('.errorMessage');
            if ($input.attr('name') == 'checkBoxAgreement') {
                if (!$input.is(':checked')) {
                    $error.show();
                    hasError = true;
                } else
                    hideError($input, $error, "Field must be filled out");
            }
        });
        if (!hasError) {
            $('#modal-1').modal('show');
        }
    })
});

function sendMessage() {
    var elements = document.forms["mainForm"].elements;
    Lobibox.notify('success', {
        title: elements.titleName.value + ' ' + elements.firstName.value + ' ' + elements.lastName.value,
        size: 'normal',
        showClass: 'flipInX',
        msg: 'Your question was sent to our support team. As soon as it\'s possible one of our administrator will ' +
            'contact you by phone ' + elements.phoneNumber.value + '.',
        closable: true,
        delay: 7000,
        delayIndicator: true,
        closeOnClick: true,
        width: 600,
        height: 'auto',
        position: "top right"
    })
}