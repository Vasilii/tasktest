/**
 * Created by Vasilii on 20.11.2015.
 */
/**
 * Show message warning
 * @param labelId
 * @param errorMessage
 */
function showError(labelId, errorMessage) {
    document.getElementById(labelId).style.color = "red";
    document.getElementById(labelId).textContent = errorMessage;
}
/**
 * Validate fields mainForm
 * @returns {boolean}
 */
function validateForm() {
    var f = 0;
    var elements = document.forms["mainForm"].elements;
    var x = elements.firstName;
    // First Name
    if (x.value == null || x.value == "") {
        showError("firstNameLabel", "Field must be filled out");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("firstNameLabel", "");
        x.style.borderColor = "#ccc";
    }
    // Last Name
    var x = elements.lastName;
    if (x.value == null || x.value == "") {
        showError("lastNameLabel", "Field must be filled out");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("lastNameLabel", "");
        x.style.borderColor = "#ccc";
    }
    // Title Name
    var x = elements.titleName;
    if (x.value == null || x.value == "") {
        showError("titlesLabel", "Field must be filled out");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("titlesLabel", "");
        x.style.borderColor = "#ccc";
    }
    var x = elements.phoneNumber;
    if (x.value == null || x.value == "") {
        showError("phoneNumberLabel", "Field must be filled out");
        x.style.borderColor = "red";
        f = 1;
    } else {
        var matches = x.value.match(/^\+(\d{1,3})[-](\d{1,3})[-](\d{1,7})/);
        if (matches == null) {
            showError("phoneNumberLabel", "Field must have phone format");
            x.style.borderColor = "red";
            f = 1;
        } else {
            showError("phoneNumberLabel", "");
            x.style.borderColor = "#ccc";
        }
    }
    var x = elements.inputBirthDate;
    var matches = x.value.match(/^(\d{2})\.(\d{2})\.(\d{4})$/);
    if (matches == null) {
        showError("inputFromDateLabel", "Field must be in date format");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("inputFromDateLabel", "");
        x.style.borderColor = "#ccc";
    }
    var x = elements.companyName;
    if (x.value == null || x.value == "") {
        showError("companyNameLabel", "Field must be filled out");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("companyNameLabel", "");
        x.style.borderColor = "#ccc";
    }
    var x = elements.questionField;
    if (x.value == null || x.value == "") {
        showError("questionFieldLabel", "Field must be filled out");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("questionFieldLabel", "");
        x.style.borderColor = "#ccc";
    }
    var x = elements.checkBoxAgreement;
    if (!x.checked) {
        showError("warningLabel", "Please fill all required fields");
        x.style.borderColor = "red";
        f = 1;
    } else {
        showError("warningLabel", "");
        x.style.borderColor = "#ccc";
    }
    if (f == 1)
        return false;
    $('#modal-1').modal('show');
}
/**
 * Function use jQuery Lobibox Plugin  and show message box
 */
function sendMessage() {
    var elements = document.forms["mainForm"].elements;
    Lobibox.notify('success', {
        title: elements.titleName.value + ' ' + elements.firstName.value + ' ' + elements.lastName.value ,
        size: 'normal',
        showClass: 'flipInX',
        msg: 'Your question was sent to our support team. As soon as it\'s possible one of our administrator will ' +
        'contact you by phone ' + elements.phoneNumber.value + '.',
        closable: true,
        delay: 7000,
        delayIndicator: true,
        closeOnClick: true,
        width: 600,
        height: 'auto',
        position: "top right"
    })
}
